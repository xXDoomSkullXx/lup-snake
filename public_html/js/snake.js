/* -----------------------------------------------------------------------------
  * Variables
  * -----------------------------------------------------------------------------
  */
 
 var snake;
 var snakeLength;
 var snakeSize;
 var snakeDirection;
 
 var food;
 
 var context;
 var screenWidth;
 var screenHeight;
 
 var gameState;
 var gameOverMenu;
 var highscore;
 
 var restartButton;
 var playHUD;
 var scoreboard;
 
var startMenu;
var startButton;

var levelsMenu;

var levelButton;
var MediumButton;
var HardButton;

var pingSound;
var appearSound;
var robotSound;
var thunderSound;

var image;
var imageSnake;

var foodGIF;
 /* -----------------------------------------------------------------------------
  * Executing Game Code
  * -----------------------------------------------------------------------------
  */
 
 gameInitialize();
 snakeInitialize();
 foodInitialize();
 setInterval(gameLoop, 1000/5);
 appearSound.play();
 
 

 /* -----------------------------------------------------------------------------
  * Game Functions
  * ------------------------------------------------------------------------------
  */
 
 function gameInitialize(){
     var canvas = element = document.getElementById("game-screen");
     context = canvas.getContext("2d");
     
     screenWidth = window.innerWidth;
     screenHeight = window.innerHeight;
     
     canvas.width = screenWidth;
     canvas.height = screenHeight;
     
     document.addEventListener("keydown", keyboardHandler);
     
     gameOverMenu = document.getElementById("gameOver");
     centerMenuPosition(gameOverMenu);
     highscore = document.getElementById("highscore");
     
     restartButton = document.getElementById("restartButton");
     restartButton.addEventListener("click", gameRestart);
     
     playHUD = document.getElementById("playHUD");
     scoreboard = document.getElementById("scoreboard");
   
    startMenu = document.getElementById("menuDisplay");
    centerMenuPosition(startMenu);
    
    startButton = document.getElementById("playButton");
    startButton.addEventListener("click", gameStart);
    
    levelsMenu = document.getElementById("LevelSelect");
    centerMenuPosition(levelsMenu);
    
    levelButton = document.getElementById("levels");
    levelButton.addEventListener("click", gameStartEasy);
    
    MediumButton = document.getElementById("Medium");
    MediumButton.addEventListener("click", gameStartMedium);   
    HardButton = document.getElementById("Hard");
    HardButton.addEventListener("click", gameStartHard);
    
    pingSound = new Audio("sounds/glass_ping.mp3");
    pingSound.preload = "auto";
    
    appearSound = new Audio("sounds/Appear.mp3");
    appearSound.preload = "auto";
    
    robotSound = new Audio("sounds/Robot.mp3");
    robotSound.preload = "auto";
    
    thunderSound = new Audio("sounds/Thunder.mp3");
    thunderSound.preload = "auto";
    
    image = document.getElementById("image");
    imageSnake = document.getElementById("imagesnake");
    
    foodGIF = document.getElementById("giffood");
    
    setState("MENU");
}
 
 function gameLoop(){
     gameDraw();
     drawScoreboard();
     if(gameState === "PLAY"){
         snakeUpdate();
         snakeDraw();
         foodDraw();
     }
 }
 
 function gameDraw(){
     context.fillStyle = "rgb(0,0,0)";  
     context.fillRect(0, 0, screenWidth, screenHeight);
     
 }
 
 function gameRestart() {
     snakeInitialize();
     foodInitialize();
     hideMenu(gameOverMenu);
     setState("LEVELS");
     robotSound.play();
 }
 
function gameStart() {
    snakeInitialize();
    foodInitialize();
    hideMenu(startMenu);
    setState("LEVELS");
    robotSound.play();
}

function gameStartEasy() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 1000/5);
    robotSound.play();
}

function gameStartMedium() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 1000/15);
    robotSound.play();
}

function gameStartHard() {
    snakeInitialize();
    foodInitialize();
    hideMenu(levelsMenu);
    setState("PLAY");
    setInterval(gameLoop, 1000/25);
    robotSound.play();
}

 /* ----------------------------------------------------------------------------
  * Snake Functions
  * -----------------------------------------------------------------------------
  */
 
 function snakeInitialize() {
     snake = [];
     snakeLength = 1;
     snakeSize = 30;
     snakeDirection = "down";
 
 
 for(var index = snakeLength-1; index >=0; index--) {
     snake.push({
         x: index,
         y: 0
     });
 }
     
 }
 
 function snakeDraw() {
     for(var index = 0; index < snake.length; index++) {
        var remainder = index % 2;
        if (remainder === 0) {
            context.fillStyle = "white";
        }
        else {
            context.fillStyle = "#7BCDE8";
        }
        context.strokeStyle = "red";
        context.strokeRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);        
        context.fillRect(snake[index].x * snakeSize, snake[index].y * snakeSize, snakeSize, snakeSize);
        context.lineWidth = 5;
     } 
 }
 
 function snakeUpdate() {
     var snakeHeadX = snake[0].x;
     var snakeHeadY = snake[0].y;
     
     if(snakeDirection === "down") {
         snakeHeadY++;
     }
     
     else if(snakeDirection === "right") {
         snakeHeadX++;
     }
     
     else if(snakeDirection === "up") {
         snakeHeadY--;
     }
     
     else if(snakeDirection === "left") {
         snakeHeadX--;
     }
     
     checkFoodCollisions(snakeHeadX, snakeHeadY);
     checkWallCollisions(snakeHeadX, snakeHeadY);
     checkSnakeCollisions(snakeHeadX, snakeHeadY);
     
     var snakeTail = snake.pop();
     snakeTail.x = snakeHeadX;
     snakeTail.y = snakeHeadY;
     snake.unshift(snakeTail);
 }
 
 /* ----------------------------------------------------------------------------
  * Food Functions
  * -----------------------------------------------------------------------------
  */
 
 function foodInitialize() {
     food = {
         x: 0,
         y: 0
     };
     setFoodPosition();
 }
 
 function foodDraw() {
    context.drawImage(image, food.x * snakeSize, food.y *snakeSize, 60, 60);
 }
 
 function setFoodPosition() {
     var randomX = Math.floor(Math.random() * screenWidth);
     var randomY = Math.floor(Math.random() * screenHeight);
     
     food.x = Math.floor(randomX / snakeSize);
     food.y = Math.floor(randomY / snakeSize);
 }
 
 /* ----------------------------------------------------------------------------
  * Input Functions
  * -----------------------------------------------------------------------------
  */
 
 function keyboardHandler(event) {
     console.log(event);
     if(event.keyCode == "39" && snakeDirection != "left") {
         snakeDirection = "right";
     }
     else if(event.keyCode == "40" && snakeDirection != "up") {
         snakeDirection = "down";
     }
     else if(event.keyCode == "38" && snakeDirection != "down") {
         snakeDirection = "up";
     }
     else if(event.keyCode == "37" && snakeDirection != "right") {
         snakeDirection = "left";
     }
 }
 
 /* ----------------------------------------------------------------------------
  * Collision Handling
  * -----------------------------------------------------------------------------
  */
 
 function checkFoodCollisions(snakeHeadX, snakeHeadY) {
     if(snakeHeadX === food.x && snakeHeadY === food.y) {
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snake.push ({
             x: 0,
             y: 0
         });
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
         snakeLength++;
            
         foodDraw();
         setFoodPosition();
         pingSound.play();
     }
    
 }
 function checkWallCollisions(snakeHeadX, snakeHeadY) {
     if(snakeHeadX * snakeSize >= screenWidth || snakeHeadX * snakeSize < 0) {
         setState("GAME OVER");
     }
     else  if(snakeHeadY * snakeSize >= screenHeight|| snakeHeadY * snakeSize < 0) {
         setState("GAME OVER");
     }
     
 }
 
 function checkSnakeCollisions(snakeHeadX, snakeHeadY){
     for(var index = 1; index < snake.length; index++){
         if(snakeHeadX === snake[index].x && snakeHeadY === snake[index].y) {
             setState("GAME OVER");
             return;
         }
     }
 }
 
 /* ----------------------------------------------------------------------------
  * Game State Handling
  * -----------------------------------------------------------------------------
  */
 
 function setState(state) {
     gameState = state;
     showMenu(state);
 }
 
 /* ----------------------------------------------------------------------------
  * Menu Functions
  * -----------------------------------------------------------------------------
  */
 
 function displayMenu(menu) {
     menu.style.visibility = "visible";
 }
 
 function hideMenu(menu){
     menu.style.visibility = "hidden";
 }
 
 function showMenu(state) {
     if(state === "GAME OVER") {
         context.fillStyle = "red"; 
         displayMenu(gameOverMenu);
         thunderSound.play();
         drawHighScoreboard();
     }
     else if(state === "PLAY"){
         displayMenu(playHUD);
     }
    else if(state === "MENU") {
        displayMenu(startMenu);
    }
    else if(state === "LEVELS") {
        displayMenu(levelsMenu);
    }
 }
 
 function centerMenuPosition(menu) {
     menu.style.top = (screenHeight / 2) - (menu.offsetHeight / 2) + "px";
     menu.style.left = (screenWidth / 2) - (menu.offsetWidth / 2) + "px";
 }
 function drawScoreboard() {
     scoreboard.innerHTML = "Length: " + snakeLength;
 }
 function drawHighScoreboard() {
     highscore.innerHTML = "Highscore: " + localStorage.getItem("highscore");
     highscore = localStorage.getItem("highscore");
     if(highscore !== null){
       if (snakeLength > highscore) {
          localStorage.setItem("highscore", snakeLength);
      }
    }
     else{
       localStorage.setItem("highscore");
    }
   }